var _ = require('lodash');

module.exports = {
	scan: function(val, mappers) {
		if(_.isPlainObject(mappers)) {
			return scanObject(val, mappers);
		}
	},
	map: function(val, mapper) {
		var string;

		if(typeof val === 'string') {
			string = val.trim();
		} else {
			string = val.element.text().trim()
		}

		var normalizedVal = mapper[string.toLowerCase()];

		// Is there a matching value (catch zeroes with undefined)
		if(normalizedVal !== undefined) {
			return normalizedVal;
		} 

		if(normalizedVal === undefined && val.passThrough) {
			return string;
		}
		
		return undefined;
	}
};

function scanArray(val, mappers) {
	var element = val.element,
	text = element.text(),
	matched;

	_.forEach(mappers, function(mapper) {
		if(_.isRegExp(mapper)) {
			matched = text.match(mapper);

			if(matched) {
				return false;
			}
		}
	});

	if(matched) {
		return matched[0];
	}

	return undefined;
};

function scanObject(val, mappers) {
	var element = val.element,
	text = element.text().trim(),
	matched;

	_.forEach(mappers, function(mapTo, mapFrom) {
		text = text.toLowerCase();

		if(_.isPlainObject(mapTo)) {
			if(!mapTo.extract || typeof mapTo.extract !== 'function') 
				throw new Error('Need extract property');

			var rgx = new RegExp(mapFrom, 'i'),
				match = text.match(rgx);

			matched = match[mapTo.extract()];

			if(_.isPlainObject(mapTo.normalize)) {
				matched = mapTo.normalize[matched];
				return false;
			}
		} else {
			var exists = text.indexOf(mapFrom) > -1;

			if(exists) {
				matched = mapTo;
			} 
		}
	});

	if(matched !== undefined) {
		return matched;
	}

	return undefined;
};