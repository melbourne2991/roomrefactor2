var _ = require('lodash'),
	moment = require('moment'),
	casters = module.exports = {};

casters.parseInt = function (val) {
	var match = val.match(/\d+/);

	if(match) {
		val = match[0];
	}

	if(val === 0 || val === '0') {
		return 0;
	}

	var parsed = parseInt(val, 10);

	if(isNaN(parsed)) {
		return undefined;
	}

	return parsed;	
};

casters.parseFloat = function(val) {
	// regex below for string  "I am string with a 44 and a 33.23 .55"
	// would match 44, 33.23 and 55 (without ".")
	var match = val.match(/-?\d+(?:\.\d+)?/);

	if(match) {
		val = match[0];
	}

	if(val === 0 || val === '0') {
		return 0;
	}

	var parsed =  parseFloat(val);

	if(isNaN(parsed)) {
		return undefined;
	}

	return parsed;
};

/*--------------Gumtree Casters----------------------*/
casters.gumtree = {};

casters.parseDate = function(val) {
	if(_.isDate(val)) return val;

	var splitDate = val.split('/'),
		dd = splitDate[0],
		mm = splitDate[1],
		yyyy = splitDate[2];

	var parsed = new Date(mm + '-'  + dd + '-' + yyyy);

	if(!validDate(parsed)) {
		return undefined
	}

	return parsed;
};

/*--------------Flatmates Casters----------------------*/
casters.flatmates = {};

casters.flatmates.parseDate = function(val) {
	if(_.isDate(val)) return val;

	console.log(val);

	return moment(val, 'MMM DD').toDate();
}


function validDate(date) {
    return date.getTime() === date.getTime();
}