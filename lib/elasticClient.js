var elasticsearch = require('elasticsearch'),
	client = new elasticsearch.Client({
		host: 'localhost:9200',
		log: 'info'		
	});	

module.exports = client;