var async = require('async'),
	utils = require('./utils'),
	cheerio = require('cheerio'),
	fs = require('fs-extra'),
	mongoose = require('mongoose'),
	Listing = require('../models/listing.js'),
	winston = require('winston'),
	regexHelper = require('./regexHelper'),
	normalizerMethods = require('./normalizerMethods'),
	imageProcessor = require('./imageProcessor'),
	casters = require('./casters'),
	_ = require('lodash');

module.exports = function(opts) {
	return new Scraper(opts);
}

function Scraper(opts) {
	var Scraper = this,
		opts = opts || {};

	if(opts.log && _.isPlainObject(opts.log)) {
		this.logger = new (winston.Logger)({
			transports: [
				new (winston.transports.Console)({
					level: opts.log.consoleLevel,
					colorize: true,
					label: this.name
				}),
				new (winston.transports.File)({ 
					filename: opts.log.path,
					level: opts.log.fileLevel
				})
			]
  		});
	} else if(_.isObject(opts.log)) {
		this.logger = opts.log;
	} else {
		this.logger = {
			info: function() {},
			error: function() {},
			warn: function() {},
			debug: function() {}
		};
	}

	Scraper.queue = async.queue(function(item, callback) {
		var source = item.source,
			crawledAt = item.crawledAt;
			url = item.url,
			$ = item.body;

		Scraper.logger.info('Queuing', url);

		var listing = new Listing({
			url: url,
			crawledAt: crawledAt,
			features: {},
			description: {},
			location: {
				coords: {}
			}
		});

		Scraper.logger.info('listing id for %s : %s', url, listing.id);

		listing.save(function(err, listing) {
			if(err) return console.log(err);

			var path = __dirname + '/../files/' + source + '/' + listing.id + '.html';

			fs.writeFile(path, $.html(), function(err) {
				if(err) {
					Scraper.logger.error('Error saving file', {id: listing.id, url: url});

					return callback();
				}
			
				var itemRef = {
					id: listing.id,
					url: url,
					listing: listing,
					source: source,
					scraper: Scraper
				};

				Scraper.scrapeFn.apply(itemRef, [$, function(scraped) {
					var	trimmed = Scraper.trimAll.apply(itemRef, [scraped]),
						cleaned = Scraper.removeEmpty.apply(itemRef, [trimmed]),
						normalized = Scraper.normalizeFn.apply(itemRef, [cleaned]),
						casted  = Scraper.castFn.apply(itemRef, [normalized]);

						Scraper.saveFn.apply(itemRef, [casted, function() {
							callback();
						}]);
				}]);
			});
		});
	}, 10);
};

Scraper.prototype.scrapeFn = function($, callback) {
	var scraper = this.scraper,
		id = this.id,
		url = this.url;

	scraper.logger.info('Scraping... ', id, {id: id, url: url});

	var scraped = scraper._scrape($);

	if(scraped.images && scraped.images.length) {
		imageProcessor(scraped.images, id, function(processed) {
			scraped.images = processed;

			callback(scraped);
		});
	} else {
		callback(scraped);
	}
};

Scraper.prototype.normalizeFn = function(obj) {
	var scraper = this.scraper,
		source = this.source,
		id = this.id,
		url = this.url,
		normalizeTo = scraper._normalize(regexHelper);

	scraper.logger.info('Normalizing... ', id, {id: id, url: url});

	_.forEach(normalizeTo, function(field, i) {
		var mapper = null, 
			mapped = null, 
			normalizedVal = null;

		// Does a normalizer exist for the field.
		if(obj[i]) {
			mapper = normalizeTo[i];

			// Is there a normalizer for the source
			if(mapper) {
				if(typeof obj[i] === 'string') {
					obj[i] = normalizerMethods['map'](obj[i], mapper);
				} else if(obj[i].method) {
					obj[i] = normalizerMethods[obj[i].method](obj[i], mapper);
				} else {
					obj[i] = normalizerMethods['scan'](obj[i], mapper);
				}
			}			
		}
	});

	return obj;
};

Scraper.prototype.castFn = function(obj) {
	var scraper = this.scraper,
		id = this.id,
		url = this.url,
		castTo = scraper._cast(casters);

	scraper.logger.info('Casting... ', id, {id: id, url: url});

	_.forEach(castTo, function(caster, i) {
		if(obj[i]) {
			obj[i] = caster.apply(scraper, [obj[i]]);
		}
	});

	return obj;
};


Scraper.prototype.saveFn = function(obj, done) {
	var scraper = this.scraper,
		id = this.id,
		url = this.url,
		listing = this.listing;

		scraper.logger.info('Saving... ', id, {id: id, url: url});

	scraper._save(obj, listing, done);
};

Scraper.prototype.normalize = function(fn) {
	this._normalize = fn;

	return this;
};

Scraper.prototype.scrape = function(fn) {
	this._scrape = fn;

	return this;
};

Scraper.prototype.cast = function(fn) {
	this._cast = fn;

	return this;
};

Scraper.prototype.save = function(fn) {
	this._save = fn;

	return this;
};

Scraper.prototype.trimAll = function(obj) {
	_.forEach(obj, function(val, i) {
		if(val !== undefined && typeof val === 'string') {
			return obj[i] = obj[i].trim();
		}
	});

	return obj;
};

Scraper.prototype.removeEmpty = function(obj) {
	_.forEach(obj, function(val, i) {
		if(val === '') {
			return obj[i] = undefined
		}
	});

	return obj;	
};
