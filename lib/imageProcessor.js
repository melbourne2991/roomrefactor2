var request = require('request');

module.exports = function(images, id, callback) {
	request.post({
		url: 'http://localhost:3300/images', 
		body: {
			id: id,
			images: images
		},
		json: true,
		timeout: 15000
	}, function(err, response, body) {
		if(body.processed) {
			callback(body.processed);
		} else {
			callback();
		}
	});
};

// var _ = require('lodash'),
// 	request = require('request'),
// 	mkdirp = require('mkdirp'),
// 	crypto = require('crypto'),
// 	getFileExtension = require(__dirname + '/getFileExtension'),
// 	fs = require('fs'),
// 	imageSizes = require(__dirname + '/imageSizes'),
// 	Pass = require('stream').PassThrough,
// 	// imageMagick = require('imagemagick-native'),
// 	sharp = require('sharp'),
// 	async = require('async');

// module.exports = function(images, id, callback) {
// 	if(!images || !images.length) callback && callback();

// 	images = _.compact(images);

// 	mkdirp(__dirname + '/../files/images/' + id, function(err) {
// 		if(err) return console.log(err);

// 		async.map(images, function(imageUrl, callback) {
// 			var imageId = crypto.randomBytes(20).toString('hex'),
// 				fileExt = getFileExtension(imageUrl).toLowerCase();

// 			if(fileExt === 'jpg' || fileExt === 'png' || fileExt === 'gif') {

// 				var writeStream, writeStreamPath, passes = {},
// 					checkStream = function(name) {
// 						return function() {
// 							passes[name].done = true;

// 							if(_.every(passes, function(pass) {
// 								return pass.done
// 							})) {
// 								callback(null, {
// 									id: imageId,
// 									ext: fileExt
// 								});
// 							}
// 						};
// 					};

// 				_.forEach(imageSizes, function(imageSize) {
// 					writeStreamPath = __dirname;
// 					writeStreamPath += '/../files/images/';
// 					writeStreamPath += id;
// 					writeStreamPath += '/';
// 					writeStreamPath += imageId ;
// 					writeStreamPath += '_' ;
// 					writeStreamPath += imageSize.name;
// 					writeStreamPath += '' ;
// 					writeStreamPath += '.' ;
// 					writeStreamPath += fileExt;

// 					writeStream = fs.createWriteStream(writeStreamPath);

// 					writeStream.on('finish', checkStream(imageSize.name));

// 					pass = new Pass();
// 					pass
// 						.pipe(sharp().resize(imageSize.dimensions.width, imageSize.dimensions.height))
// 						.pipe(writeStream);

// 					passes[imageSize.name] = {
// 						done: false,
// 						pass: pass
// 					}; 
// 				});

// 				request(imageUrl)
// 					.pipe(passes.small.pass)
// 					.pipe(passes.medium.pass)
// 					.pipe(passes.large.pass)
// 			} else {
// 				return callback();
// 			}
// 		}, function(err, results) {
// 			callback && callback(results);
// 		});		
// 	});
// };