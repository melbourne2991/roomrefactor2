var Listing = require('../models/listing'),
	moment = require('moment'),
	async = require('async');

module.exports = function(hrefs, cb) {
	async.filter(hrefs, function(href, callback) {
		Listing.findOne({url: href}, function(err, listing) {
			if(!listing) {
				return callback(true);
			}

			var crawledAt = moment(listing.crawledAt),
				now = moment()
				diff = crawledAt.diff(now, 'days');

			if(diff > 5) {
				return callback(true);
			}

			return callback(false);
		});
	}, function(results) {
		// console.log(results);
		cb(results);
	});
};