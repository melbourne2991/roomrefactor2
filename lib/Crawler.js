var cheerio = require('cheerio'),
	async = require('async'),
	_ = require('lodash'),
	utils = require('./utils'),
	RequestHandler = require('./requestHandler'),
	winston = require('winston'),
	Scraper = require('./Scraper');

module.exports = SpawnCrawler;

function SpawnCrawler(opts) {
	return new Crawler(opts)
}

function Crawler(opts) {
	opts = opts || {};
	this.name = opts.name || 'Crawler';
	this.page = opts.page || 0;
	this.delay = opts.delay || false;
	// this.fetchResponse = utils.fetchResponse.bind(this);
	
	var crawler = this,
		requestHandler = RequestHandler({
			crawler: crawler,
			delay: crawler.delay
		});

	this.fetchResponse = requestHandler.fetchResponse.bind(requestHandler);

	if(_.isPlainObject(opts.log)) {
		this.logger = new (winston.Logger)({
			transports: [
				new (winston.transports.Console)({
					level: opts.log.consoleLevel,
					colorize: true,
					label: this.name
				}),
				new (winston.transports.File)({ 
					filename: opts.log.path,
					level: opts.log.fileLevel
				})
			]
  		});
	} else if(_.isObject(opts.log)) {
		this.logger = opts.log;
	} else {
		this.logger = function() {
			return false;
		};
	}
};

CrawlerProto = Crawler.prototype;

CrawlerProto.crawl = function(urls, crawlCallback, done, delay) {
	var crawler = this;

	async.each(urls, function(url, callback) {
		crawler.logger.info('On page (%s) Fetching %s', crawler.page, url);

		crawler.fetchResponse(url, function(err, response, body) {
			var $;

			if(!err) {
				$ = cheerio.load(body);
			} else {
				return callback();
			}

			crawlCallback($, url, function() {
				return callback();
			});
		});	
	}, function() {
		crawler.logger.info('Crawl complete for page', crawler.page);
		done();
	});
};

CrawlerProto.paginate = function(trunks, paginateCallback, done) {
	var crawler = this;

	async.each(trunks, function(trunk, callback) {	
		var newCrawler = SpawnCrawler({
			log: trunk.log,
			name: trunk.name,
			page: 1,
			delay: crawler.delay
		});

		newCrawler.paginationThread(trunk.url, paginateCallback.bind(newCrawler), callback);
	}, function() {
		done();
	});
};

CrawlerProto.paginationThread = function (url, paginateCallback, done, page) {
	var crawler = this;

	crawler.logger.info('Paginator -- page', crawler.page);

	crawler.fetchResponse(url, function(err, response, body) {
		var $ = cheerio.load(body);

		paginateCallback($, function(nextPage) {
			crawler.page++;

			var next = nextPage(crawler.page);

			crawler.logger.info('NEXT PAGE >>', next)

			if(next) {
				crawler.paginationThread(next, paginateCallback, done, crawler.page);
			} else {
				done();
			}
		});
	});
};

CrawlerProto.buildHrefs = function(stem, elements) {
	if(_.isArray(stem)) {
		elements = stem;
		stem = '';
	}

	var arr = [];

	elements.each(function(i, el) {
		arr.push(stem + cheerio(el).attr('href'));
	});

	return arr;
};
