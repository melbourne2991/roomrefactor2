var request = require('request'),
	async = require('async'),
	_ = require('lodash');

var utils = module.exports = {};

var responseQueue = async.queue(function(task, callback) {
	request(task.url, function(err, res, body) {
		callback(err, {res: res, body: body});
	});
}, 20);

utils.fetchResponse = function(url, cb, attempt) {
	if(!attempt) attempt = 0;

	var crawler = this;

	if(attempt !== 0) {
		crawler.logger.warn('%s - Attempt %s', url, attempt);
	}

	responseQueue.unshift({
		url: url,
		attempt: attempt
	}, function(err, data) {
		if(err && attempt >= 3) {
			return crawler.logger.error('Error fetching page - too many retries', err.toString(), {errCode: err.code, errMsg: err.message});
		}

		if(	err &&
			(err.code === 'ECONNRESET' ||
			err.code === 'ESOCKETTIMEDOUT' ||
			err.code === 'ETIMEDOUT')
		) {
			crawler.logger.warn('Encountered %s - requeuing...', err.code);

			attempt++;

			return utils.fetchResponse.apply(crawler, [url, cb, attempt]);
		} else if(err) {
			return crawler.logger.error('Error fetching page', err.toString(), {errCode: err.code, errMsg: err.message});
		} else {
			return cb(err, data.res, data.body);
		}
	});
};

utils.eachDeep = function(object, fn) {
	var recurse = function(object) {
		_.forEach(object, function(val, i) {
			if(_.isPlainObject(val)) {
				return recurse(val);
			}

			newVal = fn(val, i);

			if(typeof newVal !== 'undefined') {
				object[i] = newVal
			}
		});
	};

	recurse(object);

	return object;
};