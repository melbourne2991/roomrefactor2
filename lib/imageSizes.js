module.exports = [
	{
		name: 'small',
		dimensions: {
			width: 450,
			height: 300
		}
	},
	{
		name: 'medium',
		dimensions: {
			width: 540,
			height: 360
		}
	},
	{
		name: 'large',
		dimensions: {
			width: 1080,
			height: 750			
		}
	}
];