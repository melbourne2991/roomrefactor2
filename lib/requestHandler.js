var request = require('request'),
	async = require('async'),
	_ = require('lodash');

module.exports = function(opts) {
	return new RequestHandler(opts);
};

function RequestHandler(opts) {
	var opts = opts || {};
	this.crawler = opts.crawler;
	this.delay = opts.delay || false;
	this.concurrency = opts.concurrency || 20;

	var delay = this.delay, concurrency = this.concurrency;

	if(delay) {
		this.responseQueue = async.queue(function(task, callback) {
			setTimeout(function() {
				request(task.url, function(err, res, body) {
					callback(err, {res: res, body: body});
				});
			}, delay);
		}, 1);
	} else {
		this.responseQueue = async.queue(function(task, callback) {
			request(task.url, function(err, res, body) {
				callback(err, {res: res, body: body});
			});
		}, concurrency);
	}
};

RequestHandler.prototype.fetchResponse = function(url, cb, attempt) {
	var crawler = this.crawler, requestHandler = this;

	if(!attempt) attempt = 0;

	if(attempt !== 0) {
		crawler.logger.warn('%s - Attempt %s', url, attempt);
	}

	requestHandler.responseQueue.unshift({
		url: url,
		attempt: attempt
	}, function(err, data) {
		if(err && attempt >= 3) {
			return crawler.logger.error('Error fetching page - too many retries', err.toString(), {errCode: err.code, errMsg: err.message});
		}

		if(	err &&
			(err.code === 'ECONNRESET' ||
			err.code === 'ESOCKETTIMEDOUT' ||
			err.code === 'ETIMEDOUT')
		) {
			crawler.logger.warn('Encountered %s - requeuing...', err.code);

			attempt++;

			return requestHandler.fetchResponse.apply(crawler, [url, cb, attempt]);
		} else if(err) {
			return crawler.logger.error('Error fetching page', err.toString(), {errCode: err.code, errMsg: err.message});
		} else {
			return cb(err, data.res, data.body);
		}
	});
};