// var async = require('async'),
// 	utils = require('./utils'),
// 	cheerio = require('cheerio'),
// 	fs = require('fs-extra'),
// 	mongoose = require('mongoose'),
// 	Listing = require('../models/listing.js'),
// 	winston = require('winston'),
// 	_ = require('lodash');

// module.exports = function(opts) {
// 	return new Scraper(opts);
// }

// function Scraper(opts) {
// 	this._scrape = null;
// 	this._clean = null;
// 	this._normalize = null;
// 	this._cast = null;
// 	this._scrape = null;

// 	if(_.isPlainObject(opts.log)) {
// 		this.logger = new (winston.Logger)({
// 			transports: [
// 				new (winston.transports.Console)({
// 					level: opts.log.consoleLevel,
// 					colorize: true,
// 					label: this.name
// 				}),
// 				new (winston.transports.File)({ 
// 					filename: opts.log.path,
// 					level: opts.log.fileLevel
// 				})
// 			]
//   		});
// 	} else if(_.isObject(opts.log)) {
// 		this.logger = opts.log;
// 	} else {
// 		this.logger = function() {
// 			return false;
// 		};
// 	}

// 	var Scraper = this;

// 	Scraper.queue = async.queue(function(item, callback) {
// 		var source = item.source,
// 			crawledAt = item.crawledAt;
// 			url = item.url,
// 			$ = item.body,
// 			id = mongoose.Types.ObjectId();

// 		Scraper.logger.info('Queuing %s with ID %s', url, id.toString());

// 		var listing = new Listing({
// 			url: url,
// 			crawledAt: crawledAt
// 		});

// 		listing.save(function(err, listing) {
// 			var path = __dirname + '/../files/' + source + '/' + listing._id.toString() + '.html';

// 			fs.writeFile(path, $.html(), function(err) {
// 				if(err) {
// 					Scraper.logger.error('Error saving file', {id: id, url: url});

// 					return callback();
// 				}
			
// 				var itemRef = {
// 					id: id.toString(),
// 					url: url,
// 					logger: Scraper.logger,
// 					scraper: Scraper,
// 					source: source
// 				};

// 				var	scraped   	= Scraper.scrapeFn.apply(itemRef, [$]),
// 					cleaned    	= Scraper.cleanFn.apply(itemRef, [scraped]),
// 					normalized 	= Scraper.normalizeFn.apply(itemRef, [cleaned]),
// 					casted 		= Scraper.castFn.apply(itemRef, [normalized]);

// 				Scraper.saveFn.apply(itemRef, [casted, id, listing, function() {
// 					Scraper.logger.info('Scrape complete, done callback called.', {id: itemRef.id, url: url});

// 					callback();
// 				}]);
// 			});
// 		});
// 	}, 10);
// };

// ScraperProto = Scraper.prototype;

// ScraperProto.scrapeFn = function($) {
// 	this.logger.info('Scraping...', id, {id: this.id});

// 	var scraper = this.scraper;

// 	return scraper._scrape($);
// };

// ScraperProto.cleanFn = function(scraped) {
// 	this.logger.info('Cleaning...', id, {id: this.id});

// 	utils.eachDeep(scraped, function(val, i) {
// 		if(i === 'value' && val !== undefined) {
// 			return val.trim();
// 		} else {
// 			return val;
// 		}
// 	});

// 	return scraped;
// };

// ScraperProto.normalizeFn = function(cleaned) {
// 	this.logger.info('Normalizing...', id, {id: this.id});

// 	var scraper = this.scraper;

// 	_.forEach(cleaned, function(val, i) {
// 		var normalizerField = scraper._normalize[i],
// 			normalizerType = null,
// 			normalizerVal = null;

// 		if(normalizerField) {
// 			normalizerType = normalizerField[scraper.source]
// 		}

// 		if(normalizerType) {
// 			normalizerVal = normalizerType[val.value];
// 		}

// 		if(normalizerVal !== undefined && normalizerVal !== null) {
// 			return cleaned[i].value = normalizerVal;
// 		}
//  	}.bind(this));

//  	return cleaned;
// };

// ScraperProto.castFn = function(normalized) {
// 	this.logger.info('Casting...', id, {id: this.id});

// 	var casted = {}, scraper = this.scraper,
// 		caster = _.merge(scraper._cast.base, scraper._cast[scraper.source]);

// 	_.forEach(normalized, function(val, i) {
// 		scraper.logger.debug('Normalizing val: %s, at index %s', val.value, i);

// 		var type = val.type;

// 		if(caster[type]) {
// 			casted[i] = caster[type](val.value);
// 		}
// 	});

// 	// _.forEach(scraper._cast.overrides, function(val, i) {
// 	// 	var caster = _.merge(scraper._cast.base, val),
// 	// 		castTo = casted[i] = {};

// 	// 	_.forEach(normalized, function(val, i) {
// 	// 		scraper.logger.debug('Normalizing val: %s, at index %s', val.value, i);

// 	// 		var type = val.type;

// 	// 		if(caster[type]) {
// 	// 			castTo[i] = caster[type](val.value);
// 	// 		}
// 	// 	});
// 	// });

// 	return casted;
// };

// ScraperProto.saveFn = function(casted, id, listing, done) {
// 	this.logger.info('Saving...', id, {id: this.id});

// 	var scraper = this.scraper;

// 	scraper._save(casted, id, listing, done);
// }

// /**** Setters ****/

// ScraperProto.scrape = function(fn) {
// 	if(!fn) return this;

// 	this._scrape = fn;
// 	return this;
// };

// ScraperProto.clean = function(fn) {
// 	if(!fn) return this;

// 	this._clean = fn;
// 	return this;
// };

// ScraperProto.normalize = function(fn) {
// 	if(!fn) return this;

// 	this._normalize = fn;
// 	return this;
// };

// ScraperProto.cast = function(fn) {
// 	if(!fn) return this;

// 	this._cast = fn;
// 	return this;
// };

// ScraperProto.save = function(fn) {
// 	if(!fn) return this;

// 	this._save = fn;
// 	return this;
// };