module.exports = function(grunt) {
	grunt.initConfig({
		mochaTest: {
			specs: {
				options: {
					ui: 'bdd',
					reporter: 'spec',
					require: __dirname + '/tests/globals.js',
					colors: true
				},
				src: ['tests/**/*.spec.js']
			}
		}
	});

	grunt.loadNpmTasks('grunt-mocha-test');
	grunt.registerTask('test', 'mochaTest:specs');
}