var schedule = require('agenda'),
	mongoose = require('mongoose'),
	gumtreeCrawler = require('./crawlers/gumtree'),
	flatmatesCrawler = require('./crawlers/flatmates');

mongoose.connect('mongodb://localhost/listaroom', function(err) {
	if(err) console.log(err);

	// gumtreeCrawler(function() {
	// 	console.log('flatmates crawl complete.');
	// });

	flatmatesCrawler(function() {
		console.log('flatmates crawl complete.')
	});
});

