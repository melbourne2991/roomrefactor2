var prompt = require('prompt'),
	request = require('request'),
	fs = require('fs-extra'),
	cheerio = require('cheerio');

prompt.start();

// prompt.get(['Scraper Name'], function(err, results) {
	// var name = results['Scraper Name'];
		// url = results['URL to Scrape'];

	var Scraper = require(__dirname + '/../scrapers/' + 'gumtree' + '.js');

	// request(url, function(err, res, body) {
	fs.readFile(__dirname + '/../files/gumtree/54eafc55f101f1b17602a464.html', 'utf8', function(err, body) {
		var $ = cheerio.load(body);

		var view = {};

		var itemRef = {
			id: '9nejfhh04u09404',
			url: 'http://somesampleurl.com/d394839',
			source: 'gumtree',
			scraper: Scraper
		};

		view.scraped = Scraper.scrapeFn.apply(itemRef, [$]),
		view.trimmed = Scraper.trimAll.apply(itemRef, [view.scraped]),
		view.cleaned = Scraper.removeEmpty.apply(itemRef, [view.trimmed]),
		view.casted  = Scraper.castFn.apply(itemRef, [view.cleaned]),
		view.normalized = Scraper.normalizeFn.apply(itemRef, [view.casted]);

		viewWhat(view);
	});
	// });
// });

function viewWhat(view) {
	console.log('View scraped, cleaned, normalized, or casted? or end');
	prompt.get(['view'], function(err, results) {
		if(results['view'] === 'end') return;

		console.log(view[results['view']]);
		viewWhat(view);
	});
}