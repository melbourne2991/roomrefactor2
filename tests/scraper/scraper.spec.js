var	Scraper = rewire(__dirname + '/../../lib/Scraper.js'), 
	winstonStub = require(__dirname + '/stubs/winston.stub'),
	scraper;

Scraper.__set__('winston', winstonStub);

describe('Scraper', function() {
	var scraper;

	beforeEach(function() {
		scraper = Scraper({
			log: {
				consoleLevel: 'info',
				fileLevel: 'info',
				path: __dirname + '/logs'
			}
		});
	});

	describe('caster', function() {
		var casterObject,
			objToCast,
			castResult,
			fnToSet;

		beforeEach(function() {
			fnToSet = function(casters) {
				return {
					bathrooms: casters.parseInt,
					bedrooms: casters.parseInt,
					price: casters.parseFloat,
					lat: casters.parseFloat,
					lon: casters.parseFloat,
					dateListed: casters.parseDate,
					dateAvail: casters.parseDate
				};
			};

			scraper.cast(fnToSet);
		});

		describe('_cast', function() {
			it('should be equal to the function passed into the setter', function() {
				expect(scraper._cast).to.deep.equal(fnToSet);
			});
		});

		describe('castFn', function() {
			it('should only cast properties for which there is a corresponding field in the caster object', function() {
				objToCast = {
					noCorresponding: '52'
				};

				castResult = scraper.castFn.apply({
					scraper: scraper,
					id: 'testid',
					url: 'http://testurl.com/test'
				}, [objToCast]);

				expect(castResult.noCorresponding).to.be.a('string');
			});

			describe('parseInt/parseFloat', function() {
				it('should return NaNs as undefined', function() {
					objToCast = {
						bathrooms: 'thisisnotanumber'
					};

					castResult = scraper.castFn.apply({
						scraper: scraper,
						id: 'testid',
						url: 'http://testurl.com/test'
					}, [objToCast]);

					expect(castResult.bathrooms).to.be.undefined;
				});

				// it('should call the missing definition notifier when casted to an invalid number/float', function() {
				// 	objToCast = {
				// 		bedrooms: 'thisisnotavalidnumber',
				// 		lon: 'thisisnotavalidfloat'
				// 	};

				// 	var scraperMock = sinon.mock(scraper);

				// 	scraperMock.expects('missingDefinitionNotifier').twice();

				// 	castResult = scraper.castFn.apply({
				// 		scraper: scraper,
				// 		id: 'testid',
				// 		url: 'http://testurl.com/test'
				// 	}, [objToCast]);			

				// 	scraperMock.verify();
				// });
			});

			describe('parseDate', function() {
				it('should return invalid dates as undefined', function() {
					objToCast = {
						dateAvail: 'thisisnotavaliddate'
					};

					castResult = scraper.castFn.apply({
						scraper: scraper,
						id: 'testid',
						url: 'http://testurl.com/test'
					}, [objToCast]);			

					expect(castResult.dateAvail).to.be.undefined;
				});

				// it('should call the missing definition notifier when casted to an invalid date', function() {
				// 	objToCast = {
				// 		dateAvail: 'thisisnotavaliddate'
				// 	};

				// 	var scraperMock = sinon.mock(scraper);

				// 	scraperMock.expects('missingDefinitionNotifier').once();

				// 	castResult = scraper.castFn.apply({
				// 		scraper: scraper,
				// 		id: 'testid',
				// 		url: 'http://testurl.com/test'
				// 	}, [objToCast]);			


				// 	scraperMock.verify();				
				// });	
			});		
		})
	});

	describe('normalizer', function() {
		var normalizerObject,
			objToNormalize,
			fnToSet;

		beforeEach(function() {
			fnToSet = function() {
				return {
					gender: {

							'no preference': 0,
							'male': 1,
							'female': 2

					},
					furnished: {

							'yes': 1,
							'no': 0

					},
					parking: {

							'street': 2,
							'off street': 2,
							'covered': 3,
							'none': 0

					},
					type: {

							'houseshare': 2,
							'flatshare': 1,
							'other shared accomodation': 3

					},
					smoking: {

							'yes': 1,
							'no': 0

					},
					pets: {

							'yes': 1,
							'no': 0

					},
					country: {

							'australia': 0

					}
				};
			};

			scraper.normalize(fnToSet);
		});

		describe('_normalize', function() {
			it('should be equal to the function passed into the setter', function() {
				expect(scraper._normalize).to.deep.equal(fnToSet);
			});
		});

		describe('normalizeFn', function() {
			it('should only normalize properties which have a corresponding field in the normalizer object', function() {
				objToNormalize = {
					noCorresponding: 'testing'
				};

				var normalizeResult = scraper.normalizeFn.apply({
					scraper: scraper,
					id: 'testid',
					url: 'http://testurl.com/test',
					source: 'gumtree'
				}, [objToNormalize]);

				expect(normalizeResult.noCorresponding).to.equal('testing');				
			});

			it('should return properties with a corresponding field, but without a matching value as undefined and call the missing definition', function() {
				objToNormalize = {
					gender: 'elephant'
				};

				var scraperMock = sinon.mock(scraper);

				// scraperMock.expects('missingDefinitionNotifier').once().withArgs('gender', 'gumtree', 'elephant');

				var normalizeResult = scraper.normalizeFn.apply({
					scraper: scraper,
					id: 'testid',
					url: 'http://testurl.com/test',
					source: 'gumtree'
				}, [objToNormalize]);

				expect(normalizeResult.gender).to.be.undefined;

				scraperMock.verify();
			});

			it('should not change the value of a property if there is no normalizer that exists for it', function() {
				objToNormalize = {
					noNormalizer: 'doistaythesame'
				};

				var normalizeResult = scraper.normalizeFn.apply({
					scraper: scraper,
					id: 'testid',
					url: 'http://testurl.com/test',
					source: 'gumtree'
				}, [objToNormalize]);

				expect(normalizeResult.noNormalizer).to.equal('doistaythesame');
			});
		});
	});

	describe('normalize then cast', function() {
		scraper = Scraper({
			log: {
				consoleLevel: 'info',
				fileLevel: 'info',
				path: __dirname + '/logs'
			}
		});

		var normalizeFnToSet = function() {
			return {
				gender: {
					'no preference': 0,
					'male': 1,
					'female': 2
				},
				furnished: {
					'yes': 1,
					'no': 0
				},
				parking: {
					'street': 2,
					'off street': 2,
					'covered': 3,
					'none': 0
				},
				type: {
					'houseshare': 2,
					'flatshare': 1,
					'other shared accomodation': 3
				},
				smoking: {
					'yes': 1,
					'no': 0
				},
				pets: {
					'yes': 1,
					'no': 0
				},
				country: {
					'australia': 0
				},
				price: {
					'negotiable': 0
				}
			};
		},
		castFnToSet = function(casters) {
			return {
				bathrooms: casters.parseInt,
				bedrooms: casters.parseInt,
				price: casters.parseFloat,
				lat: casters.parseFloat,
				lon: casters.parseFloat,
				dateListed: casters.parseDate,
				dateAvail: casters.parseDate
			};
		},
		objToNormalizeAndCast = {
			price: 'Negotiable',
			bedrooms: '5',
			lat: '-0.3345',
			desc_title: 'This is a great apartment'
		};

		scraper.normalize(normalizeFnToSet);
		scraper.cast(castFnToSet);

		var normalizeResult = scraper.normalizeFn.apply({
				scraper: scraper,
				id: 'testid',
				url: 'http://testurl.com/test',
				source: 'gumtree'
			}, [objToNormalizeAndCast]),

			castResult = scraper.castFn.apply({
				scraper: scraper,
				id: 'testid',
				url: 'http://testurl.com/test',
				source: 'gumtree'				
			}, [normalizeResult]);


		it('should result in a valid lat value parsed to a float', function() {
			expect(castResult.lat).to.be.a('number');
		});

		it('should result in price "Negotiable" being undefined', function() {
			expect(castResult.price).to.equal(0);
		});
	});
});