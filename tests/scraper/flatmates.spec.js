var FlatmatesScraper = require(__dirname + '/../../scrapers/flatmates'),
	moment = require('moment'),
	cheerio = require('cheerio'),
	fs = require('fs-extra');

describe('Flatmates Scraper', function() {
	var flatmatesScraper;

	beforeEach(function() {
		flatmatesScraper = FlatmatesScraper();
	});

	describe('test file 1', function() {
		var $, scraped, trimmed, cleaned, normalized, casted;

		beforeEach(function(done) {
			fs.readFile(__dirname + '/testHtmls/flatmates/test1.html', 'utf8', function(err, data) {
				$ = cheerio.load(data);

				var itemRef = {
					id: 0,
					url: 'http://test.com/test',
					listing: null,
					source: 'flatmates',
					scraper: flatmatesScraper
				};

				flatmatesScraper.scrapeFn.apply(itemRef, [$, function(scraped) {
					trimmed = flatmatesScraper.trimAll.apply(itemRef, [scraped]);
					cleaned = flatmatesScraper.removeEmpty.apply(itemRef, [trimmed]);
					normalized = flatmatesScraper.normalizeFn.apply(itemRef, [cleaned]);
					casted  = flatmatesScraper.castFn.apply(itemRef, [normalized]);

					done();
				}]);
			});
		});

		describe('Date Avail "Now"', function() {
			it('should be 01-01-2000', function() {
				var dateAvail = moment(casted.dateAvail).format('MM-DD-YYYY');

				expect(dateAvail).to.eql('01-01-2000');
			});
		});

		describe('Price', function() {
			it('should be float 170', function() {
				expect(casted.price).to.eql(170);
			});
		});

		describe('Gender', function() {
			it('should be int 0', function() {
				expect(casted.gender).to.eql(0);
			});
		});

		describe('Bedrooms', function() {
			it('should be int 3', function() {
				expect(casted.bedrooms).to.eql(3);
			});
		});

		describe('Bathrooms', function() {
			it('should be int 1', function() {
				expect(casted.bathrooms).to.eql(1);
			});
		});

		describe('Smoking', function() {
			it('should be int 0', function() {
				expect(casted.smoking).to.eql(0);
			});
		});

		describe('Parking', function() {
			it('should be int 1', function() {
				expect(casted.parking).to.eql(1);
			})
		});

		describe('Internet access', function() {
			it('should be int 1', function() {
				expect(casted.internet).to.eql(1);
			});
		});

		describe('Furnished', function() {
			it('should be int 1', function() {
				expect(casted.furnished).to.eql(1);
			});
		});

		describe('Type', function() {
			it('should be int 2', function() {
				expect(casted.type).to.eql(2);
			});
		});

		describe('Lat', function() {
			it('should be float -27.5605139', function() {
				expect(casted.lat).to.eql(-27.9770653);
			});
		});

		describe('images - scraped', function() {
			it('should be an array of image ids', function() {
				expect(casted.images[2].ext).to.eql('jpg');
			});
		});	

		describe('suburb - casted', function() {
			it('should be Southport', function() {
				expect(casted.suburb).to.eql('Southport');
			});
		});
	});



	describe('test file 2', function() {
		var $, scraped, trimmed, cleaned, normalized, casted;

		beforeEach(function(done) {
			fs.readFile(__dirname + '/testHtmls/flatmates/test2.html', 'utf8', function(err, data) {
				$ = cheerio.load(data);

				var itemRef = {
					id: 0,
					url: 'http://test.com/test',
					listing: null,
					source: 'flatmates',
					scraper: flatmatesScraper
				};

				flatmatesScraper.scrapeFn.apply(itemRef, [$, function(scraped) {
					trimmed = flatmatesScraper.trimAll.apply(itemRef, [scraped]);
					cleaned = flatmatesScraper.removeEmpty.apply(itemRef, [trimmed]);
					normalized = flatmatesScraper.normalizeFn.apply(itemRef, [cleaned]);
					casted  = flatmatesScraper.castFn.apply(itemRef, [normalized]);

					done();
				}]);
			});
		});

		describe('Date Avail', function() {
			it('should be 03-28-2015', function(done) {
				var dateAvail = moment(casted.dateAvail).format('MM-DD-YYYY');

				expect(dateAvail).to.eql('03-28-2015');
				done();
			});
		});

		describe('Price', function() {
			it('should be float 130', function(done) {
				expect(casted.price).to.eql(130);
				done();
			});
		});

		describe('Gender', function() {
			it('should be int 2', function() {
				expect(casted.gender).to.eql(2);
			});
		});

		describe('Bedrooms', function() {
			it('should be int 3', function() {
				expect(casted.bedrooms).to.eql(3);
			});
		});

		describe('Bathrooms', function() {
			it('should be int 2', function() {
				expect(casted.bathrooms).to.eql(2);
			});
		});

		describe('Smoking', function() {
			it('should be int 0', function() {
				expect(casted.smoking).to.eql(0);
			});
		});

		describe('Parking', function() {
			it('should be int 1', function() {
				expect(casted.parking).to.eql(1);
			})
		});

		describe('Internet access', function() {
			it('should be int 1', function() {
				expect(casted.internet).to.eql(1);
			});
		});

		describe('Furnished', function() {
			it('should be undefined', function() {
				expect(casted.furnished).to.eql(undefined);
			});
		});

		describe('Type', function() {
			it('should be int 1', function() {
				expect(casted.type).to.eql(1);
			});
		});

		describe('Lat', function() {
			it('should be float -27.9201846', function() {
				expect(casted.lat).to.eql(-27.9201846);
			});
		});
	});
});

