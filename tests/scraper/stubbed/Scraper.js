var	Scraper = rewire(__dirname + '/../../../lib/Scraper'),
	winstonStub = require(__dirname + '/../stubs/winston.stub'),
	listingStub = require(__dirname + '/../stubs/Listing.stub'),
	fsStub = require(__dirname + '/../stubs/fs.stub');

Scraper.__set__('Listing', listingStub);
Scraper.__set__('winston', winstonStub);
Scraper.__set__('fs', fsStub);

module.exports = Scraper;

