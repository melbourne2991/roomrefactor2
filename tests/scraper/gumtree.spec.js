var GumtreeScraper = require(__dirname + '/../../scrapers/gumtree'),
	cheerio = require('cheerio'),
	fs = require('fs-extra');

describe('Gumtree "shared_accommodation" Scraper', function() {
	var gumtreeScraper;

	beforeEach(function() {
		gumtreeScraper = GumtreeScraper();
	});

	describe('test file 1', function() {
		var $, scraped, trimmed, cleaned, normalized, casted;

		beforeEach(function(done) {
			fs.readFile(__dirname + '/testHtmls/gumtree/test1.html', function(err, data) {
				$ = cheerio.load(data);

				var itemRef = {
					id: 0,
					url: 'http://test.com/test',
					listing: null,
					source: 'gumtree',
					scraper: gumtreeScraper
				};

				gumtreeScraper.scrapeFn.apply(itemRef, [$, function(scrapedRes) {
					scraped = scrapedRes;
					trimmed = gumtreeScraper.trimAll.apply(itemRef, [scraped]);
					cleaned = gumtreeScraper.removeEmpty.apply(itemRef, [trimmed]);
					normalized = gumtreeScraper.normalizeFn.apply(itemRef, [cleaned]);
					casted  = gumtreeScraper.castFn.apply(itemRef, [normalized]);

					done();
				}]);
			});
		});

		describe('parking - normalized', function() {
			it('should be int 0', function() {
				expect(normalized.parking).to.eql(0);
			});
		});

		describe('suburb - casted', function() {
			it('should be Inglewood', function() {
				expect(casted.suburb).to.eql('Inglewood');
			});
		});
	});

	describe('test file 2', function() {
		var $, scraped, trimmed, cleaned, normalized, casted;
		
		beforeEach(function(done) {
			this.timeout(5000);
			
			fs.readFile(__dirname + '/testHtmls/gumtree/test2.html', function(err, data) {
				$ = cheerio.load(data);

				var itemRef = {
					id: 0,
					url: 'http://test.com/test',
					listing: null,
					source: 'gumtree',
					scraper: gumtreeScraper
				};

				gumtreeScraper.scrapeFn.apply(itemRef, [$, function(scrapedRes) {
					scraped = scrapedRes;
					trimmed = gumtreeScraper.trimAll.apply(itemRef, [scraped]);
					cleaned = gumtreeScraper.removeEmpty.apply(itemRef, [trimmed]);
					normalized = gumtreeScraper.normalizeFn.apply(itemRef, [cleaned]);
					casted  = gumtreeScraper.castFn.apply(itemRef, [normalized]);

					done();
				}]);
			});
		});	
		
		describe('images - scraped', function() {
			it('should be an array of image ids', function() {
				expect(casted.images[0].ext).to.eql('jpg');
			});
		});	
	});


});
