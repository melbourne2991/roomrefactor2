var rewire = require('rewire'),
	chai = require('chai'),
	sinon = require('sinon');

global.sinon = sinon;
global.expect = chai.expect;
global.rewire = rewire;