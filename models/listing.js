var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	esClient = require(__dirname + '/../lib/elasticClient'),
	moment = require('moment'),
	_ = require('lodash'),
	ObjectId = Schema.Types.ObjectId;

var ListingSchema = new Schema({
	createdAt: {type: Date, default: Date.now},
	updatedAt: Date,
	crawledAt: Date,
	source: String,
	url: String,	
	dateListed: Date,
	dateAvail: Date,
	price: Number,
	bond: Number,
	term: String,
	description: {
		title: String,
		user: String,
		full: String
	},
	features: {
		// Numbers map to different strings
		smoking: Number,
		internet: Number,
		gender: Number,
		furnished: Number,
		parking: Number,
		pets: Number,
		// Number represents quantity
		bathrooms: Number,
		bedrooms: Number,
		type: {type: Number}
	},
	location: {
		// Number maps to different strings
		country: Number,
		address: String,
		suburb: String,
		coords: {
			lat: Number,
			lon: Number
		}
	},
	images: Array
});

ListingSchema.pre('save', function(next) {
	this.updatedAt = Date.now();
	next();
});

ListingSchema.methods.saveToElastic = function(cb) {
	var doc = _.clone(this._doc, true),
		id = this.id;

	if(doc.dateAvail) {
		doc.dateAvail = moment(doc.dateAvail).format('YYYYMMDD[T]HHmmssZ');
	}

	if(doc.dateListed) {
		doc.dateListed = moment(doc.dateListed).format('YYYYMMDD[T]HHmmssZ');
	}

	if(!_.compact(doc.images).length) {
		doc.images = undefined;
	}

	esClient.index({
		index: 'listings',
		type: 'basic-listing',
		id: id,
		body: {
			dateListed: doc.dateListed,
			dateAvail: doc.dateAvail,
			price: doc.price,
			bond: doc.bond,
			term: doc.term,
			origin: doc.source,
			url: doc.url,
			location: doc.location,
			features: doc.features,
			description: doc.description,
			images: doc.images
		}
	}, function(err) {
		if(err) {
			cb(err);
		} else {
			cb(null);
		}
	});
};

module.exports = mongoose.model('Listing', ListingSchema);

