var Crawler = require('../lib/Crawler.js'),
	getCrawlable = require('../lib/getCrawlable.js'),
	GumtreeScraper = require('../scrapers/gumtree'),
	mongoose = require('mongoose'),
	moment = require('moment');

	module.exports = initCrawl;

var gumtreeScraper = GumtreeScraper({
	log: {
		path: __dirname + '/../logs/scrapers/gumtree.log',
		consoleLevel: 'debug',
		fileLevel: 'info'
	}
});

var trunks = [
	{
		url: 'http://www.gumtree.com.au/s-flatshare-houseshare/c18294', 
		name: 'Trunk 1', 
		log: {
			path: __dirname + '/../logs/crawlers/gumtree/s-flatshare-houseshare.log',
			consoleLevel: 'info',
			fileLevel: 'info',
			mongoLevel: 'info'
		}
	}
];

function initCrawl(cb) {
	Crawler().paginate(trunks, function($, next) {
		var hrefs = this.buildHrefs('http://gumtree.com.au', $('.rs-ad-title.h-elips a'));

		var pageCrawler = this;

		getCrawlable(hrefs, function(crawlable) {
			if(!crawlable.length) {
				pageCrawler.logger.info('No crawlable links on this page.');

				return next(getNext($));
			}

			Crawler({log: pageCrawler.logger, page: pageCrawler.page}).crawl(crawlable, function($, url, done) {
				gumtreeScraper.queue.push({
					body: $, 
					source: 'gumtree', 
					url: url,
					crawledAt: Date.now()
				});

				done();
			}, function() {
				return next(getNext($));
			});
		});
	}, function() {
		cb();
	});
}

function getNext($) {
	return function(count) {
		if($('.rs-paginator-btn.next').length) {
			return 'http://gumtree.com.au' + $('.rs-paginator-btn.next').attr('href');
		} else {
			return false;
		}
	};
}