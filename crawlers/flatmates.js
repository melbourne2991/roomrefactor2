var Crawler = require('../lib/Crawler.js'),
	getCrawlable = require('../lib/getCrawlable.js'),
	FlatmatesScraper = require('../scrapers/flatmates'),
	mongoose = require('mongoose'),
	moment = require('moment');

	module.exports = initCrawl;

var flatmatesScraper = FlatmatesScraper({
	log: {
		path: __dirname + '/../logs/scrapers/flatmates.log',
		consoleLevel: 'debug',
		fileLevel: 'info'
	}
});

var trunks = [
	{
		url: 'https://flatmates.com.au/', 
		name: 'Flatmates', 
		log: {
			path: __dirname + '/../logs/crawlers/flatmates.log',
			consoleLevel: 'info',
			fileLevel: 'info',
			mongoLevel: 'info'
		}
	}
];

function initCrawl(cb) {
	Crawler({delay: 3000}).paginate(trunks, function($, next) {
		var hrefs = this.buildHrefs('http://flatmates.com.au', $('li.photo a.flag'));

		var pageCrawler = this;

		getCrawlable(hrefs, function(crawlable) {
			if(!crawlable.length) {
				pageCrawler.logger.info('No crawlable links on this page.');

				return next(getNext($));
			}

			// flat mates doesn't like us crawling too fast so add a 3000ms delay.
			Crawler({log: pageCrawler.logger, page: pageCrawler.page}).crawl(crawlable, function($, url, done) {
				flatmatesScraper.queue.push({
					body: $, 
					source: 'flatmates', 
					url: url,
					crawledAt: Date.now()
				});

				done();
			}, function() {
				return next(getNext($));
			});
		});
	}, function() {
		cb();
	});
}

function getNext($) {
	return function(count) {
		return 'http://flatmates.com.au' + $('#listings_footer #pagination a:contains("Next")').attr('href');
	};
}