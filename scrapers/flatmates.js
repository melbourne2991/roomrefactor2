var	Scraper = require('../lib/Scraper.js'),
	moment = require('moment'),
	_ = require('lodash'),
	utils = require('../lib/utils');

module.exports = function(opts) {
	var flatmatesScraper = Scraper(opts);

	flatmatesScraper
		.scrape(function($) {
			var images = [], suburb;

			images.push($('#img1').attr('src'));

			_.forEach($('#photothumbs a'), function(a, i) {
				a = $(a);

				if(a.attr('href').length) images.push(a.attr('href'));
			});

			suburb = $('#relatedlinks p a:first-child').text().replace('rooms in ', '');

			return 	{
				dateListed: undefined,
				dateAvail: {method: 'map', element: $('#leftcol .thirds .third3 h3'), passThrough: true},
				// dateAvail: {method: 'map', element: $('#leftcol .thirds .third3 h3'), passThrough: true},
				price: $('#leftcol .thirds .third1 h3').text(),
				term: $('#leftcol .thirds .third3 p'),
				desc_title: $('title').text(), 
				desc_user: $('description').html(),
				desc_full: $('#leftcol').text(),
				// // Numbers map to different strings
				smoking: {method: 'scan', element: $('#leftcol .columns')},
				internet: {method: 'scan', element: $('#leftcol .columns')},
				gender: {method: 'scan', element: $('#leftcol .columns')}, 
				furnished: {method: 'scan', element: $('#leftcol .columns')},
				parking: {method: 'scan', element: $('#leftcol .columns')},
				pets: {method: 'scan', element: $('#leftcol .columns')},
				// // // Number represents quantity
				bathrooms: {method: 'scan', element: $('#leftcol .columns')},
				bedrooms: {method: 'scan', element: $('#leftcol .columns')},
				type: {method: 'scan', element: $('#leftcol .columns')},
				// // Number maps to different strings
				country: 'Australia',
				address: undefined,
				suburb: suburb,
				lat: $('#rightcol #lat').val(),
				lon: $('#rightcol #lon').val(),
				images: images
			};
		})
		.normalize(function(regexHelper) {
			return {
				bedrooms: {
					'\\d+(?=.*bedroom)': {
						extract: regexHelper.returnIndex(0)
					}
				},
				bathrooms: {
					'\\d+(?=.*bathroom)': {
						extract: regexHelper.returnIndex(0)
					}
				},
				dateAvail: {
					'now': new Date('01-01-2000')
				},
				internet: {
					'internet access available': 1
				},
				type: {
					'(?:\\d+ bedroom) (.*)': {
						extract: regexHelper.returnIndex(1),
						normalize: {
							'house': 1,
							'apartment': 2
						}
					}
				},
				gender: {
					'any gender': 0,
					'prefer males': 1,
					'prefer females': 2
				},
				furnished: {
					'furnished with bed': 1,
					'No': 0
				},
				parking: {
					'street': 2,
					'off street': 2,
					'covered': 3,
					'parking space available': 1,
					'none': 0
				},
				smoking: {
					'prefer non-smokers': 0
				},
				pets: {
					'yes': 1,
					'no': 0
				},
				country: {
					'australia': 0
				}
			};
		})		
		.cast(function(casters) {
			return {
				bathrooms: casters.parseInt,
				bedrooms: casters.parseInt,
				price: casters.parseFloat,
				lat: casters.parseFloat,
				lon: casters.parseFloat,
				dateListed: casters.flatmates.parseDate,
				dateAvail: casters.flatmates.parseDate
			};
		})
		.save(function(casted, listing, done) {
			listing.dateListed = casted.dateListed;
			listing.dateAvail = casted.dateAvail;
			
			listing.price = casted.price;
			listing.bond = casted.bond;
			listing.term = casted.term;
			
			listing.description.title = casted.desc_title;
			listing.description.user = casted.desc_user;
			listing.description.full = casted.desc_full;

			listing.features.smoking = casted.smoking;
			listing.features.internet = casted.internet;
			listing.features.gender = casted.gender;
			listing.features.furnished = casted.furnished;
			listing.features.parking = casted.parking;
			listing.features.pets = casted.pets;
			listing.features.bathrooms = casted.bathrooms;
			listing.features.bedrooms = casted.bedrooms;
			listing.features.type = casted.type;

			listing.location.country = casted.country;
			listing.location.address = casted.address;
			listing.location.suburb = casted.suburb;

			listing.location.coords.lat = casted.lat;
			listing.location.coords.lon = casted.lon;

			listing.images = casted.images;

			listing.save(function(err) {
				if(err) {
					console.log(err);
				} else {
					listing.saveToElastic(function(err) {
						if(err) return console.log(err);
						return console.log('Successfully saved to elastic');
					});
				}

				done();
			});
		});	

	return flatmatesScraper;
}