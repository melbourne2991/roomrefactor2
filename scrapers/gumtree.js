var	Scraper = require('../lib/Scraper.js'),
	moment = require('moment'),
	_ = require('lodash'),
	utils = require('../lib/utils');

module.exports = function(opts) {
	var gumtreeScraper = Scraper(opts);

	gumtreeScraper
		.scrape(function($) {
			var images = [], suburb;

			_.forEach($('#ad-gallery-thumbs .carousel-items li span'), function(span, i) {
				span = $(span);
				images.push(span.attr('data-href-large'));
			});

			suburb = $('#breadcrumb li a:contains("Real Estate")')
							.parent()
							.prev()
							.find('a')
							.text();

			if(!suburb.length) suburb = undefined;

			return 	{
				dateListed: $(':contains("Date Listed:")').siblings('dd').text(),
				dateAvail: $('#c-shared_accomodation\\.availabilitystartdate_tdt').text(),
				price: $('#ad-price .h-elips').text().replace(/\$/, ''),
				term: undefined,
				desc_title: $('#ad-title').text(), 
				desc_user: $('#ad-description').text(),
				desc_full: $('#ad-body-inner').text(),
				// Numbers map to different strings
				smoking: $('#c-shared_accomodation\\.smoking_s').text(),
				internet: undefined,
				gender: $('#c-shared_accomodation\\.preferredgender_s').text(), 
				furnished: $('#c-shared_accomodation\\.furnished_s').text(),
				parking: $('#c-shared_accomodation\\.parking_s').text(),
				pets: $('#c-shared_accomodation\\.petsallowed_s').text(),
				// Number represents quantity
				bathrooms: $('#c-shared_accomodation\\.numberbathrooms_s').text(),
				bedrooms: $('#c-shared_accomodation\\.numberbedrooms_s').text(),
				type: $('#c-shared_accomodation\\.dwellingtype_s-wrapper dd').text(),
				// Number maps to different strings
				country: 'Australia',
				address: $('#ad-map .j-google-map-link').attr('data-address'),
				suburb: suburb,
				lat: $('#ad-map .j-google-map-link').attr('data-lat'),
				lon: $('#ad-map .j-google-map-link').attr('data-lng'),
				images: images
			};
		})
		.cast(function(casters) {
			return {
				bathrooms: casters.parseInt,
				bedrooms: casters.parseInt,
				price: casters.parseFloat,
				lat: casters.parseFloat,
				lon: casters.parseFloat,
				dateListed: casters.parseDate,
				dateAvail: casters.parseDate
			};
		})
		.normalize(function() {
			return {
				gender: {
					'no preference': 0,
					'male': 1,
					'female': 2
				},
				furnished: {
					'yes': 1,
					'no': 0
				},
				parking: {
					'street': 1,
					'off street': 2,
					'covered': 3,
					'none': 0
				},
				type: {
					'houseshare': 2,
					'flatshare': 1,
					'other shared accomodation': 3
				},
				smoking: {
					'yes': 1,
					'no': 0
				},
				pets: {
					'yes': 1,
					'no': 0
				},
				country: {
					'australia': 0
				}
			};
		})
		.save(function(casted, listing, done) {
			listing.dateListed = casted.dateListed;
			listing.dateAvail = casted.dateAvail;
			
			listing.price = casted.price;
			listing.bond = casted.bond;
			listing.term = casted.term;
			
			listing.description.title = casted.desc_title;
			listing.description.user = casted.desc_user;
			listing.description.full = casted.desc_full;

			listing.features.smoking = casted.smoking;
			listing.features.internet = casted.internet;
			listing.features.gender = casted.gender;
			listing.features.furnished = casted.furnished;
			listing.features.parking = casted.parking;
			listing.features.pets = casted.pets;
			listing.features.bathrooms = casted.bathrooms;
			listing.features.bedrooms = casted.bedrooms;
			listing.features.type = casted.type;

			listing.location.country = casted.country;
			listing.location.address = casted.address;
			listing.location.suburb = casted.suburb;

			listing.location.coords.lat = casted.lat;
			listing.location.coords.lon = casted.lon;

			listing.images = casted.images;

			listing.save(function(err) {
				if(err) {
					console.log(err);
				} else {
					listing.saveToElastic(function(err) {
						if(err) return console.log(err);
						return console.log('Successfully saved to elastic');
					});
				}

				done();
			});
		});	

	return gumtreeScraper;
}