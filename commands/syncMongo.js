var listingModel = require(__dirname + '/../models/listing'),
	Mongo = require('mongodb'),
	async = require('async'),
	esClient = require(__dirname + '/../lib/elasticClient'),
	moment = require('moment'),
	MongoClient = Mongo.MongoClient;

MongoClient.connect('mongodb://localhost:27017/listaroom', function(err, db) {
	if(err) return console.log(err);

	console.log('Connected to mongo server...');

	var queue = async.queue(function(doc, callback) {
		if(doc.dateAvail) {
			doc.dateAvail = moment(doc.dateAvail).format('YYYYMMDD[T]HHmmssZ');
		}

		if(doc.dateListed) {
			doc.dateListed = moment(doc.dateListed).format('YYYYMMDD[T]HHmmssZ');
		}

		esClient.index({
			index: 'listings',
			type: 'basic-listing',
			id: doc._id.toString(),
			body: {
				dateListed: doc.dateListed,
				dateAvail: doc.dateAvail,
				price: doc.price,
				bond: doc.bond,
				term: doc.term,
				origin: doc.source,
				url: doc.url,
				location: doc.location,
				features: doc.features,
				description: doc.description
			}
		}, function(err) {
			callback();
		});
	}, 20);

	queue.drain = function() {
		console.log('Sync complete');
	};

	var listings = db.collection('listings'),
		cursor = listings.find({});

		cursor.on('error', function(err) {
			console.log(err);
		});

		cursor.on('data', function(doc) {
			console.log('Streaming...' + doc._id.toString());
			console.log('adding to queue');

			queue.push(doc, function(err) {
				if(err) return console.log(err);
			});
		});

		cursor.once('end', function() {
			db.close();
		});
});