var elasticSearch = require('elasticsearch'),
	prompt = require('prompt'),
	client = new elasticSearch.Client({
		host: 'localhost:9200',
		log: 'info'
	});


prompt.start();

prompt.get(['create index or mapping?'], function(err, result) {
	if(result['create index or mapping?'] === 'index') {
		prompt.get(['index name?'], function(err, result) {
			createIndex(result['index name?']);
		});
	}

	if(result['create index or mapping?'] === 'mapping') {
		prompt.get(['index name?', 'mapping name?'], function(err, result) {
			createMapping(result['index name?'], result['mapping name?']);
		});
	}
});

function createIndex(indexName) {
	client.indices.create({index: indexName}, function(err, listingsIndex) {				
		if(err) return console.log(err);

		return console.log('index created');
	});
};

function createMapping(indexName, mappingName, cb) {
	client.indices.putMapping({
		index: indexName, 
		type: mappingName,
		body: {
			properties: {
				origin: {
					type: 'string',
					index: 'not_analyzed'
				},
				url: {
					type: 'string',
					index: 'not_analyzed'
				},
				dateListed: {
					type: 'date',
					format: 'basic_date_time_no_millis'
				},
				dateAvail: {
					type: 'date',
					format: 'basic_date_time_no_millis'
				},
				price: {
					type: 'float'
				},
				bond: {
					type: 'float'
				},
				term: {
					type: 'string'
				},
				description: {
					type: 'object',
					properties: {
						title: {
							type: 'string'
						},
						user: {
							type: 'string'
						},
						full: {
							type: 'string'
						}
					}
				},
				features: {
					type: 'object',
					properties: {
						smoking: {
							type: 'integer'
						},
						internet: {
							type: 'integer'
						},
						gender: {
							type: 'integer'
						},
						furnished: {
							type: 'integer'
						},
						parking: {
							type: 'integer'
						},
						pets: {
							type: 'integer'
						},
						bathrooms: {
							type: 'integer'
						},
						bedrooms: {
							type: 'integer'
						},
						type: {
							type: 'integer'
						}
					}
				},
				location: {
					type: 'object',
					properties: {
						country: {
							type: 'integer'
						},
						address: {
							type: 'string'
						},
						suburb: {
							type: 'string'
						},
						coords: {
							type: 'geo_point',
							fielddata: {
								format: 'compressed',
								precision: '3m'
							}
						}
					}
				},
				images: {
					properties: {
						id: {
							type: 'string'
						},
						ext: {
							type: 'string'
						}
					}
				}
			}
		}
	}, function(err) {
		if(err) return console.log(err);

		console.log('mapping created.');
	});	
};