var elasticSearch = require('elasticsearch'),
	prompt = require('prompt'),
	client = new elasticSearch.Client({
		host: 'localhost:9200',
		log: 'info'
	});


prompt.start();

prompt.get(['delete index or mapping?'], function(err, result) {
	if(result['delete index or mapping?'] === 'index') {
		prompt.get(['index name?'], function(err, result) {
			deleteIndex(result['index name?']);
		});
	}

	if(result['delete index or mapping?'] === 'mapping') {
		prompt.get(['index name?', 'mapping name?'], function(err, result) {
			deleteMapping(result['index name?'], result['mapping name?']);
		});
	}
});

function deleteMapping(indexName, mappingName) {
	client.indices.deleteMapping({
		index: indexName,
		type: mappingName
	}, function(err) {
		if(err) return console.log(err);
		return console.log('mapping deleted');
	});	
};

function deleteIndex(indexName) {
	client.indices.delete({
		index: indexName
	}, function(err) {
		if(err) return console.log(err);
		return console.log('index deleted');
	});
};