var fs = require('graceful-fs'),
	async = require('async'),
	scraper = require(__dirname + '/../scrapers/gumtree'),
	cheerio = require('cheerio'),
	mongoose = require('mongoose'),
	filesPath = __dirname + '/../files/gumtree';

mongoose.connect('mongodb://localhost/listaroom', function(err) {
	if(err) console.log(err);

	fs.readdir(filesPath, function(err, files) {
		async.eachLimit(files, 10, function(file, callback) {
			var fullPath = filesPath + '/' + file;

			fs.readFile(fullPath, 'utf8', function(err, data) {
				if(err) return console.log(err);

				var $ = cheerio.load(data);

				scraper.queue.push({
					body: $, 
					source: 'gumtree', 
					url: 'http://gumtree.com/',
					crawledAt: Date.now()
				}, function() {
					callback();
				});
			});
		}, function(err) {
			console.log('done');
		});	
	});
});	

